package com.company.project.vo.resp;

import com.company.project.entity.SysRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * UserOwnRoleRespVO
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class UserOwnRoleRespVO {
    @Schema(description = "所有角色集合")
    private List<SysRole> allRole;
    @Schema(description = "用户所拥有角色集合")
    private List<String> ownRoles;
}
