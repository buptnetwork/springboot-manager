package com.company.project.vo.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * DeptRespNodeVO
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class DeptRespNodeVO {
    @Schema(description = "组织id")
    private String id;

    @Schema(description = "组织编码")
    private String deptNo;

    @Schema(description = "组织名称")
    private String title;

    @Schema(description = "组织名称")
    private String label;

    @Schema(description = "组织父级id")
    private String pid;

    @Schema(description = "组织状态")
    private Integer status;

    @Schema(description = "组织关系id")
    private String relationCode;

    @Schema(description = "是否展开 默认不展开(false)")
    private boolean spread = true;

    @Schema(description = "是否选中")
    private boolean checked = false;

    private boolean disabled = false;

    @Schema(description = "子集")
    private List<?> children;

    public String getLabel() {
        return title;
    }

}
