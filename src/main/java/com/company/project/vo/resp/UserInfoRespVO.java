package com.company.project.vo.resp;

//import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * UserInfoRespVO
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class UserInfoRespVO {
    @Schema(description = "用户id")
    private String id;
    @Schema(description = "账号")
    private String username;
    @Schema(description = "手机号")
    private String phone;
    @Schema(description = "昵称")
    private String nickName;
    @Schema(description = "真实姓名")
    private String realName;
    @Schema(description = "所属机构id")
    private String deptId;
    @Schema(description = "所属机构名称")
    private String deptName;

}
