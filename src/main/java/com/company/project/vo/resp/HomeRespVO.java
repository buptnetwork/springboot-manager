package com.company.project.vo.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * HomeRespVO
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class HomeRespVO {
    @Schema(description = "用户信息")
    private UserInfoRespVO userInfo;
    @Schema(description = "目录菜单")
    private List<PermissionRespNode> menus;

}