package com.company.project.vo.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * PermissionRespNode
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class PermissionRespNode {
    @Schema(description = "id")
    private String id;
    @Schema(description = "菜单权限名称")
    private String title;

    @Schema(description = "菜单权限标识，shiro 适配restful")
    private String perms;

    @Schema(description = "接口地址")
    private String url;

    @Schema(description = "icon")
    private String icon;

    private String target;

    @Schema(description = "父级id")
    private String pid;

    @Schema(description = "父级名称")
    private String pidName;

    @Schema(description = "菜单权限类型(1:目录;2:菜单;3:按钮)")
    private Integer type;

    @Schema(description = "排序码")
    private Integer orderNum;

    @Schema(description = "是否展开 默认不展开(false)")
    private boolean spread = true;

    @Schema(description = "是否选中 默认false")
    private boolean checked;
    private List<?> children;


}
