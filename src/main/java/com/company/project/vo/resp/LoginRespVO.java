package com.company.project.vo.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * LoginRespVO
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Data
public class LoginRespVO {
    @Schema(description = "token")
    private String accessToken;
    @Schema(description = "用户名")
    private String username;
    @Schema(description = "用户id")
    private String id;
    @Schema(description = "电话")
    private String phone;
    @Schema(description = "用户所拥有的菜单权限(前后端分离返回给前端控制菜单和按钮的显示和隐藏)")
    private List<PermissionRespNode> list;
}
