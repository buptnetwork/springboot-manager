package com.company.project.controller.api;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * api test示例
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年5月11日
 */
@RestController
@RequestMapping("/app/api")
@Tag(name = "test")
public class TestController {


    @PostMapping("/login")
    @Operation(summary = "登录接口")
    public SaResult login() {
        // 第1步，先登录上
        StpUtil.login(10001);
        // 第2步，获取 Token  相关参数
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        // 第3步，返回给前端
        return SaResult.data(tokenInfo);
    }


    @GetMapping("/getCurUserInfo")
    @Operation(summary = "获取当前登录人信息示例")
    public void getAppUserInfo() {
        //拿userId与userName
        String userId = StpUtil.getLoginIdAsString();
    }
}
