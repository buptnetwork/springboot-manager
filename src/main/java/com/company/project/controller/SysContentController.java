package com.company.project.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.company.project.common.utils.AssertUtil;
import com.company.project.entity.SysContentEntity;
import com.company.project.service.SysContentService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.List;


/**
 * 文章管理
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Tag(name = "文章管理")
@RestController
@RequestMapping("/sysContent")
public class SysContentController {
    @Resource
    private SysContentService sysContentService;


    @Operation(summary = "新增")
    @PostMapping("/add")
    @SaCheckPermission("sysContent:add")
    public void add(@RequestBody SysContentEntity sysContent) {
        sysContentService.save(sysContent);
    }

    @Operation(summary = "删除")
    @DeleteMapping("/delete")
    @SaCheckPermission("sysContent:delete")
    public void delete(@RequestBody @Parameter(description = "id集合") List<String> ids) {
        sysContentService.removeByIds(ids);
    }

    @Operation(summary = "更新")
    @PutMapping("/update")
    @SaCheckPermission("sysContent:update")
    public void update(@RequestBody SysContentEntity sysContent) {
        AssertUtil.isStringNotBlank(sysContent.getId(), "id不能为空");
        sysContentService.updateById(sysContent);
    }

    @Operation(summary = "查询分页数据")
    @PostMapping("/listByPage")
    @SaCheckPermission("sysContent:list")
    public IPage<SysContentEntity> findListByPage(@RequestBody SysContentEntity sysContent) {
        LambdaQueryWrapper<SysContentEntity> queryWrapper = Wrappers.lambdaQuery();
        //查询条件示例
        if (!StringUtils.isEmpty(sysContent.getTitle())) {
            queryWrapper.like(SysContentEntity::getTitle, sysContent.getTitle());
        }
        return sysContentService.page(sysContent.getQueryPage(), queryWrapper);
    }
}
