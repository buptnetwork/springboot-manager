package com.company.project.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.company.project.common.exception.BusinessException;
import com.company.project.common.utils.AssertUtil;
import com.company.project.entity.SysDictDetailEntity;
import com.company.project.service.SysDictDetailService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.List;


/**
 * 字典明细管理
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Tag(name = "字典明细管理")
@RestController
@RequestMapping("/sysDictDetail")
public class SysDictDetailController {
    @Resource
    private SysDictDetailService sysDictDetailService;

    @Operation(summary = "新增")
    @PostMapping("/add")
    @SaCheckPermission("sysDict:add")
    public void add(@RequestBody SysDictDetailEntity sysDictDetail) {
        AssertUtil.isStringNotBlank(sysDictDetail.getValue(), "字典值不能为空");
        LambdaQueryWrapper<SysDictDetailEntity> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictDetailEntity::getValue, sysDictDetail.getValue());
        queryWrapper.eq(SysDictDetailEntity::getDictId, sysDictDetail.getDictId());
        SysDictDetailEntity q = sysDictDetailService.getOne(queryWrapper);
        AssertUtil.isNull(q, "字典名称-字典值已存在");
        sysDictDetailService.save(sysDictDetail);
    }

    @Operation(summary = "删除")
    @DeleteMapping("/delete")
    @SaCheckPermission("sysDict:delete")
    public void delete(@RequestBody @Parameter(description = "id集合") List<String> ids) {
        sysDictDetailService.removeByIds(ids);
    }

    @Operation(summary = "更新")
    @PutMapping("/update")
    @SaCheckPermission("sysDict:update")
    public void update(@RequestBody SysDictDetailEntity sysDictDetail) {
        AssertUtil.isStringNotBlank(sysDictDetail.getValue(), "字典值不能为空");
        LambdaQueryWrapper<SysDictDetailEntity> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictDetailEntity::getValue, sysDictDetail.getValue());
        queryWrapper.eq(SysDictDetailEntity::getDictId, sysDictDetail.getDictId());
        SysDictDetailEntity q = sysDictDetailService.getOne(queryWrapper);

        sysDictDetailService.updateById(sysDictDetail);
    }


    @Operation(summary = "查询列表数据")
    @PostMapping("/listByPage")
    @SaCheckPermission("sysDict:list")
    public IPage<SysDictDetailEntity> findListByPage(@RequestBody SysDictDetailEntity sysDictDetail) {
        if (StringUtils.isEmpty(sysDictDetail.getDictId())) {
            return new Page<>();
        }
        return sysDictDetailService.listByPage(sysDictDetail.getQueryPage(), sysDictDetail.getDictId());
    }

}
